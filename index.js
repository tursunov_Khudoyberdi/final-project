let argsArr = document.getElementById("args");
let finalText = document.getElementById("finalAnswer");

document.querySelectorAll("[id=nums]").forEach(e => {
  e.addEventListener("click", function () {
    let arg = this.innerText;
    if (arg === "C") {
      argsArr.innerText = "";
      finalText.innerText = "0";
      return;
    }
    if (arg === "=") {
      finalText.innerText = eval(argsArr.innerText);
      if (finalText.textContent === 0) {
        finalText.innerText = 0;
      }
      argsArr.textContent = "";
    } else {
      argsArr.textContent += arg;
      return;
    }
  });
});
